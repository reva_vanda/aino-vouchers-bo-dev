'use strict'

const Model = use('Model')

class Setting extends Model {
	static get rules() {
		return {
			code: 'required',
			key: 'required',
			value: 'required',
			serialized: 'required'
		}
	}
}

module.exports = Setting