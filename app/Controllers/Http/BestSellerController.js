'use strict'

const Reward = use('App/Models/Reward')
const Loyalty = use('App/Models/Loyalty')
const LoyaltyDetails = use('App/Models/LoyaltyDetails')
const BestSeller = use('App/Models/BestSeller')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const moment = require('moment')

class BestSellerController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('best-seller.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async datatable({request, response, auth, view, session}) {
		const formData = request.post()

		let tableDefinition

		if (auth.user.user_role == '1' || auth.user.user_role == '2') {
			tableDefinition = {
				sTableName: 'best_sellers',
				sFromSql: "best_sellers inner join rewards on best_sellers.reward_id = rewards.id",
				sSelectSql: ['best_sellers.id', 'rewards.name', 'best_sellers.date_start', 'best_sellers.date_end', 'best_sellers.status'],
				aSearchColumns: ['rewards.name', 'best_sellers.date_start', 'best_sellers.date_end', 'best_sellers.status'],
				dbType: 'postgres'
			}
		} else {
			tableDefinition = {
				sTableName: 'best_sellers',
				sFromSql: "best_sellers inner join rewards on best_sellers.reward_id = rewards.id",
				sSelectSql: ['best_sellers.id', 'rewards.name', 'best_sellers.date_start', 'best_sellers.date_end', 'best_sellers.status'],
				aSearchColumns: ['rewards.name', 'best_sellers.date_start', 'best_sellers.date_end', 'best_sellers.status'],
				sWhereAndSql: "principal_id = '"+ auth.user.principal_id +"'",
				dbType: 'postgres'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				no,
				moment(selectRow[x]['date_start']).format('DD/MM/YYYY'),
				moment(selectRow[x]['date_start'], 'HH:mm:ss').format('HH:mm'),
				moment(selectRow[x]['date_end']).format('DD/MM/YYYY'),
				moment(selectRow[x]['date_end'], 'HH:mm:ss').format('HH:mm'),
				selectRow[x]['name'],
				selectRow[x]['status'] == 1 ? "<span class='label label-success'>Active</span>" : "<span class='label label-danger'>Non Active</span>",
				"<div class='text-center'>\
					<a href='./best-seller/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}

	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('best-seller.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { date_start, date_end, selected_rewards, status } = request.only(['date_start', 'date_end', 'selected_rewards', 'status'])
		
		let formCheck = {
			date_start: date_start,
			date_end: date_end
		}
		
		const rules = {
			date_start: 'required',
			date_end: 'required'
		}
		
		const validation = await validateAll(formCheck, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let rewardJson = JSON.parse(selected_rewards)

			for(let x in rewardJson) {
				let formData = {
					user_id: auth.user.id,
					principal_id: auth.user.principal_id,
					principal_code: auth.user.principal_code,
					date_start: date_start,
					date_end: date_end,
					reward_id: parseInt(rewardJson[x]['id']),
					status: parseInt(status),
					created_by: auth.user.id,
					updated_by: auth.user.id
				}

				let bestseller = await BestSeller.query().select('id', 'date_start', 'date_end').where('reward_id',rewardJson[x]['id']).first()

				if(bestseller) {
					let newStart = moment(date_start).format('YYYY-MM-DD HH:mm:ss')
					let fnewStart = moment(newStart).valueOf()

					let start = moment(bestseller.date_start).format('YYYY-MM-DD HH:mm:ss')
					let fstart = moment(start).valueOf()

					let end = moment(bestseller.date_end).format('YYYY-MM-DD HH:mm:ss')
					let fend = moment(end).valueOf()

					if(fnewStart >= fstart && fnewStart <= fend) {
					} else {
						await BestSeller.create(formData)
					}
				} else {
					await BestSeller.create(formData)
				}
			}
			
			session.flash({ notification: 'Best seller added', status: 'success' })
			response.header('Content-type', 'application/json')
			response.type('application/json')
			let data = {
				code: '200',
				message: 'Best seller added',
				data: [],
			}

			return response.send(data);
		}
	}

	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let bestseller = await BestSeller.find(params.id)
		bestseller['date_start'] = moment(bestseller.date_start).format('YYYY-MM-DD HH:mm')
		bestseller['date_end'] = moment(bestseller.date_end).format('YYYY-MM-DD HH:mm')

		if (bestseller) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

			let reward = await Reward.query().select('id', 'name').where('id', bestseller.reward_id).first()
		
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}
			
			let template = view.render('best-seller.edit', {
				meta: meta,
				bestseller: bestseller,
				reward: reward
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'Best seller not found', status: 'danger' })
			return response.redirect('/best-seller')
		}
	}

	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const { date_start, date_end, status } = request.only(['date_start', 'date_end', 'status'])
		
		let bestseller = await BestSeller.find(params.id)
		
		let formData = {
			date_start: date_start,
			date_end: date_end,
			status: parseInt(status)
		}
		
		const rules = {
			date_start: 'required',
			date_end: 'required',
			status: 'required'
		}
		
		const validation = await validateAll(formData, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (bestseller) {
				await BestSeller.query().where('id', params.id).update(formData)

				session.flash({ notification: 'Best seller updated', status: 'success' })
				return response.redirect('/best-seller')
			} else {
				session.flash({ notification: 'Best seller not found', status: 'danger' })
				return response.redirect('/best-seller')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		let bestSeller = await BestSeller.find(formData.id)
		if (bestSeller){
			try {
				await bestSeller.delete()
				session.flash({ notification: 'Best seller success deleted', status: 'success' })
				return response.redirect('/best-seller')
			} catch (e) {
				session.flash({ notification: 'Best seller cannot be delete', status: 'danger' })
				return response.redirect('/best-seller')
			}
		} else {
			session.flash({ notification: 'Best seller cannot be delete', status: 'danger' })
			return response.redirect('/best-seller')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('BestSeller', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let bestSeller = await BestSeller.find(formData.item[i])
				try {
					await bestSeller.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Best seller success deleted', status: 'success' })
			return response.redirect('/best-seller')
		} else {
			session.flash({ notification: 'Best seller cannot be deleted', status: 'danger' })
			return response.redirect('/best-seller')
		}
	}

	async getDataReward({request, response, auth, view, session}) {
		const { limit, page } = request.only([ 'limit', 'page' ])
		let rewards

		if (auth.user.user_role == '1' || auth.user.user_role == '2') {
			rewards = await Reward.query().select('rewards.id', 'rewards.name', 'loyalty_details.expiry_date', 'loyalty_details.price')
				.leftJoin('loyalty_details', 'loyalty_details.id', 'rewards.loyalty_detail_id')
				.leftJoin('loyalties', 'loyalties.id', 'loyalty_details.loyalty_id')
				.where('rewards.status', 1)
				.orderBy('rewards.name', 'asc')
				.paginate(page, limit)
		} else {
			rewards = await Reward.query().select('rewards.id', 'rewards.name', 'loyalty_details.expiry_date', 'loyalty_details.price')
				.leftJoin('loyalty_details', 'loyalty_details.id', 'rewards.loyalty_detail_id')
				.leftJoin('loyalties', 'loyalties.id', 'loyalty_details.loyalty_id')
				.where('loyalties.principal_id', auth.user.principal_id)
				.where('rewards.status', 1)
				.orderBy('rewards.name', 'asc')
				.paginate(page, limit)
		}

		let dataReward = rewards.toJSON()

		if(rewards) {
			let data = {
				code: '2000',
				message: 'Reward Found',
				data: dataReward['data']
			}
			return response.send(data)
		} else {
			let data = {
				code: '4000',
				message: 'Reward Not Found',
				data: []
			}
			return response.send(data)
		}
	}
}

module.exports = BestSellerController
