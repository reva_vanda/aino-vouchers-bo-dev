'use strict'

const TransactionBurn = use('App/Models/TransactionBurn')
const User = use('App/Models/User')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')
const moment = require('moment');

class TransactionBurnController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('TransactionBurn', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('transaction-burn.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}

	async datatable({request, response, auth, view, session}) {
		const formData = request.post()

		let tableDefinition

		if (auth.user.user_role == '1') {
			tableDefinition = {
				sTableName: 'transaction_burns',
				sFromSql: "transaction_burns inner join users on transaction_burns.user_id = users.id inner join rewards on transaction_burns.reward_id = rewards.id inner join loyalty_details on rewards.loyalty_detail_id = loyalty_details.id",
				sSelectSql: ["loyalty_details.price", "loyalty_details.selling_price", "transaction_burns.id", "user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "transaction_burns.status"],
				aSearchColumns: ["user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "status"],
				dbType: 'postgres'
			}
		} if(auth.user.user_role == '2') {
			tableDefinition = {
				sTableName: 'transaction_burns',
				sFromSql: "transaction_burns inner join users on transaction_burns.user_id = users.id inner join rewards on transaction_burns.reward_id = rewards.id inner join loyalty_details on rewards.loyalty_detail_id = loyalty_details.id",
				sSelectSql: ["loyalty_details.price", "transaction_burns.id", "user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "transaction_burns.status"],
				aSearchColumns: ["user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "status"],
				dbType: 'postgres'
			}
		} else {
			tableDefinition = {
				sTableName: 'transaction_burns',
				sFromSql: "transaction_burns inner join users on transaction_burns.user_id = users.id",
				sSelectSql: ["transaction_burns.id", "user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "transaction_burns.status"],
				aSearchColumns: ["user_name", "user_id", "issuer_name", "card_number", "reward_used", "reward_name", "reward_id", "date", "status"],
				sWhereAndSql: "users.principal_id = '"+ auth.user.principal_id +"'",
				dbType: 'postgres'
			}
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 1
		if(auth.user.user_role == '1'){
			for(let x in selectRow) {
				fdata.push([
					no,
					selectRow[x]['user_id'],
					selectRow[x]['user_name'],
					selectRow[x]['issuer_name'],
					selectRow[x]['card_number'],
					selectRow[x]['reward_used'],
					selectRow[x]['reward_name'],
					selectRow[x]['reward_id'],
					moment(selectRow[x]['date']).format('DD/MM/YYYY'),
					moment(selectRow[x]['date']).format('HH:mm'),
					selectRow[x]['price'],
					selectRow[x]['selling_price'],
					selectRow[x]['status']
				])
				no++
			}
		} if(auth.user.user_role == '2'){
			for(let x in selectRow) {
				fdata.push([
					no,
					selectRow[x]['user_id'],
					selectRow[x]['user_name'],
					selectRow[x]['issuer_name'],
					selectRow[x]['card_number'],
					selectRow[x]['reward_used'],
					selectRow[x]['reward_name'],
					selectRow[x]['reward_id'],
					moment(selectRow[x]['date']).format('DD/MM/YYYY'),
					moment(selectRow[x]['date']).format('HH:mm'),
					selectRow[x]['selling_price'],
					selectRow[x]['status']
				])
				no++
			}
		} else {
			for(let x in selectRow) {
				fdata.push([
					no,
					selectRow[x]['user_id'],
					selectRow[x]['user_name'],
					selectRow[x]['issuer_name'],
					selectRow[x]['card_number'],
					selectRow[x]['reward_used'],
					selectRow[x]['reward_name'],
					selectRow[x]['reward_id'],
					moment(selectRow[x]['date']).format('DD/MM/YYYY'),
					moment(selectRow[x]['date']).format('HH:mm'),
					selectRow[x]['status']
				])
				no++
			}
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
}

module.exports = TransactionBurnController
