'use strict'

const Setting = use('App/Models/Setting')
const Theme = use('App/Models/Theme')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')

class ThemeController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Theme', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let theme = await Theme.query().where('principal_id', auth.user.principal_id).first()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
			
		let template = view.render('theme.index', {
			meta: meta,
			themes: theme
		})
		
		return await minifyHTML.minify(template)
	}
	
	async update({request, response, auth, view, session}) {
		const req = request.all()

		let theme = await Theme.query().where('principal_id', auth.user.principal_id).first()

		let nameLogo
		let nameBanner

		const logoPic = request.file('logo', {
		    types: ['image'],
		    size: '10mb'
		})

		if(logoPic != null) {
			const type = request.file('logo').subtype;
			nameLogo = `${new Date().getTime()}_themelogo_${auth.user.id}.${type}`;
			
			await logoPic.move(Helpers.publicPath('/uploads/images/themelogo'), {
				name: nameLogo
			})

			if (!logoPic.moved()) {
			    return logoPic.error()
			}
		}

		const bannerPic = request.file('banner', {
		    types: ['image'],
		    size: '10mb'
		})

		if(bannerPic != null) {
			const type = request.file('banner').subtype;
			nameBanner = `${new Date().getTime()}_themebanner_${auth.user.id}.${type}`;
			
			await bannerPic.move(Helpers.publicPath('/uploads/images/themebanner'), {
				name: nameBanner
			})

			if (!bannerPic.moved()) {
			    return bannerPic.error()
			}
		}

		if(theme) {
			let formData

			if(logoPic == null && bannerPic == null) {
				formData = {
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id
				}
			} else if(logoPic == null && bannerPic != null) {
				formData = {
					banner: '/uploads/images/themebanner/' + nameBanner,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id
				}
			} else if(logoPic != null && bannerPic == null) {
				formData = {
					logo: '/uploads/images/themelogo/' + nameLogo,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id
				}
			} else if(logoPic != null && bannerPic != null) {
				formData = {
					logo: '/uploads/images/themelogo/' + nameLogo,
					banner: '/uploads/images/themebanner/' + nameBanner,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id
				}
			}

			await Theme.query().where('user_id', auth.user.id).update(formData)
		} else {
			let formData

			if(logoPic == null && bannerPic == null) {
				formData = {
					user_id: auth.user.id,
					principal_id: auth.user.principal_id,
					principal_code: auth.user.principal_code,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id,
					created_by: auth.user.id
				}
			} else if(logoPic == null && bannerPic != null) {
				formData = {
					user_id: auth.user.id,
					principal_id: auth.user.principal_id,
					principal_code: auth.user.principal_code,
					banner: '/uploads/images/themebanner/' + nameBanner,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id,
					created_by: auth.user.id
				}
			} else if(logoPic != null && bannerPic == null) {
				formData = {
					user_id: auth.user.id,
					principal_id: auth.user.principal_id,
					principal_code: auth.user.principal_code,
					logo: '/uploads/images/themelogo/' + nameLogo,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id,
					created_by: auth.user.id
				}
			} else if(logoPic != null && bannerPic != null) {
				formData = {
					user_id: auth.user.id,
					principal_id: auth.user.principal_id,
					principal_code: auth.user.principal_code,
					logo: '/uploads/images/themelogo/' + nameLogo,
					banner: '/uploads/images/themebanner/' + nameBanner,
					color_one: req.color_one ? req.color_one : null,
					color_two: req.color_two ? req.color_two : null,
					color_three: req.color_three ? req.color_three : null,
					color_four: req.color_four ? req.color_four : null,
					updated_by: auth.user.id,
					created_by: auth.user.id
				}
			}

			await Theme.create(formData)
		}

		session.flash({ notification: 'Theme updated', status: 'success' })
		return response.redirect('/theme')
	}
}

module.exports = ThemeController