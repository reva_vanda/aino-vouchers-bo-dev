'use strict'

// MODULES
const Validation = require('joi')
const http = require('axios')
const Base64 = require('js-base64').Base64;
// MODELS
const ApiModel = use('App/Models/ApiService')
//STATICS
const PartnerAPI = require('../../../../resources/statics/partnerapi')

class TadaService {
	async getToken(){
		let resp
		let token = {
			eligible : false,
			token : "NA",
			message : ""
		}
		const options = {
			url: PartnerAPI.TADA.STAGING.url + PartnerAPI.TADA.STAGING.apis.getToken.endpoint,
			method: PartnerAPI.TADA.STAGING.apis.getToken.method,
			data: {
				"username" : PartnerAPI.TADA.STAGING.username,
				"password" : PartnerAPI.TADA.STAGING.password,
				"grant_type" : "password",
				"scope" : "offline_access"
			},
			headers: {
				'Content-Type' : PartnerAPI.TADA.STAGING.apis.getToken.content_type,
				'Authorization' : 'Basic ' + Base64.encode(PartnerAPI.TADA.STAGING.api_key + ':' + PartnerAPI.TADA.STAGING.api_secret)
			},
			timeout: PartnerAPI.TADA.STAGING.timeouts.middle
		}

		resp = await http(options).then(function (response) {
			try{
				token.eligible = true
				token.token = response.data.access_token
			}catch(e){
				token.message = e.message
			}
		})
		.catch(function (e) {
			try{
				if (e.response.data.error_description){
					token.message = e.response.data.error_description
				} else {
					token.message = e.message
				}
			} catch (e2) {
				token.message = e2.message
			}
		})
		return token
	}

	async getItems(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}
		let token = null
		// Define validation scheme
		const schema = Validation.object().keys({
			categoryId : Validation.string().min(1).max(50).required(),
			//programId  : Validation.string().min(1).max(50).required(), // always put if it's a GET
			page :  Validation.string().regex(/^\d+$/).required(),
			perPage :  Validation.string().regex(/^\d+$/).required()
		})
		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				options.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})
		if (validationprc){
			validationprc.programId = PartnerAPI.TADA.STAGING.program_id
			token = await this.getToken()
		}
		if (token.eligible){
			const options = {
				url: PartnerAPI.TADA.STAGING.url + PartnerAPI.TADA.STAGING.apis.getItems.endpoint,
				method: PartnerAPI.TADA.STAGING.apis.getItems.method,
				params: validationprc,
				headers: {
					'Content-Type' : PartnerAPI.TADA.STAGING.apis.getItems.content_type,
					'Authorization' : 'Bearer ' + token.token
				},
				timeout: PartnerAPI.TADA.STAGING.timeouts.middle
			}
			const resp = await http(options).then(function (response) {
				result.success = true
				result.data = response.data
			})
			.catch(function (e) {
				try{
					if (e.response.data){
						result.message = e.response.data
					} else {
						result.message = e.message
					}
				} catch (e2) {
					result.message = e2.message
				}
			})
		}
		return result
	}

	async getCategories(customs){
		let result = {
			success : false,
			data : {},
			message : ""
		}
		let token = null
		// Define validation scheme
		const schema = Validation.object().keys({
			//programId  : Validation.string().min(1).max(50).required(), // always put if it's a GET
			page :  Validation.string().regex(/^\d+$/).required(),
			perPage :  Validation.string().regex(/^\d+$/).required()
		})
		// Validate the scheme, don't change tis const !
		const validationprc = await Validation.validate(customs, schema, (err, value) => { 
			let validation = null
			if(err) {
				options.message  = err.message[0]
			} else {
				validation = value
			}
			return validation
		})
		if (validationprc){
			validationprc.programId = PartnerAPI.TADA.STAGING.program_id
			token = await this.getToken()
		}
		if (token.eligible){
			const options = {
				url: PartnerAPI.TADA.STAGING.url + PartnerAPI.TADA.STAGING.apis.getCategories.endpoint,
				method: PartnerAPI.TADA.STAGING.apis.getCategories.method,
				params: validationprc,
				headers: {
					'Content-Type' : PartnerAPI.TADA.STAGING.apis.getCategories.content_type,
					'Authorization' : 'Bearer ' + token.token
				},
				timeout: PartnerAPI.TADA.STAGING.timeouts.middle
			}
			const resp = await http(options).then(function (response) {
				result.success = true
				result.data = response.data
			})
			.catch(function (e) {
				try{
					if (e.response.data){
						result.message = e.response.data
					} else {
						result.message = e.message
					}
				} catch (e2) {
					result.message = e2.message
				}
			})
		}
		return result
	}
}

module.exports = new TadaService