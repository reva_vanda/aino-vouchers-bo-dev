'use strict'

const Setting = use('App/Models/Setting')
const { validate } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const fs = require('fs')
const path = require('path')
const Helpers = use('Helpers')
const Logger = use('Logger')
const apiConnect = require('../Helper/ApiService.js')

class ApiController {
	async index({request, response, auth}) {
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let apiconnect = await apiConnect.apiAccess('piT45IVPZpEPijLp0AiCSHF5Zol9lRSG', 'MqWYAerVn0uvPDsWEvjoSoy3KJKyWrp8Q9zChVg9ZK4vdM6mlrVyT00Bx7CkydpB', 'customer')
		let data = {
			title: 'Welcome Aino',
			description: 'Aino API Services',
			version: '1.0',
			copyright: 'PT AINO',
			apiconnect: apiconnect
		}
		return response.send(data)
	}
	
	async getSetting({request, response, auth}) {
		const { id } = request.get()
		
		let settings
		if (id) {
			let arrId = id.split(',')
			settings = await Setting.query().select('id', 'code', 'key', 'value', 'serialized').whereIn('id', arrId).fetch()
		} else {
			settings = await Setting.query().select('id', 'code', 'key', 'value', 'serialized').fetch()
		}
		
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let data = {
			code: '2000',
			message: 'Success get all setting',
			data: settings.toJSON()
		}
		return response.send(data)
	}
	
	async getSettingById({params, request, response, auth}) {
		response.header('Content-type', 'application/json')
		response.type('application/json')
		let settings = await Setting.query().select('id', 'code', 'key', 'value', 'serialized').where('id', params.id).first()
		let data = {
			code: '2000',
			message: 'Success get setting by id',
			data: settings.toJSON()
		}
		return response.send(data)
	}
}

module.exports = ApiController