'use strict'

const Setting = use('App/Models/Setting')
const PrincipalConversion = use('App/Models/PrincipalConversion')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')

class PrincipalConversionController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('PrincipalConversion', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let principalConversion = await PrincipalConversion.query().where('principal_id', auth.user.principal_id).first()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
			
		let template = view.render('principal-conversion.index', {
			meta: meta,
			principalConversions: principalConversion
		})
		
		return await minifyHTML.minify(template)
	}
	
	async update({request, response, auth, view, session}) {
		const formData = request.all()
		
		let principalConversion = await PrincipalConversion.query().where('principal_id', auth.user.principal_id).first()

		if(principalConversion) {
			await PrincipalConversion.query().where('user_id', auth.user.id).update({
				amount: formData.amount,
				principal_id: auth.user.principal_id,
				principal_code: auth.user.principal_code,
			})
		} else {
			await PrincipalConversion.create({
				user_id: auth.user.id,
				amount: formData.amount,
				principal_id: auth.user.principal_id,
				principal_code: auth.user.principal_code,
				created_by: auth.user.id,
				updated_by: auth.user.id,
			})
		}

		session.flash({ notification: 'Conversion updated', status: 'success' })
		return response.redirect('/principal-conversion')
	}
}

module.exports = PrincipalConversionController