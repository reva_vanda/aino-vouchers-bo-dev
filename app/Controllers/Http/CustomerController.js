'use strict'

const User = use('App/Models/User')
const RoleUser = use('App/Models/RoleUser')
const Customer = use('App/Models/Customer')
const Setting = use('App/Models/Setting')
const { validate, validateAll } = use('Validator')
const Hash = use('Hash')
const Database = use('Database')
const QueryBuilder = require('./Helper/DatatableBuilder.js')
const CheckAuth = require('./Helper/CheckAuth.js')
const minifyHTML = require('./Helper/MinifyHTML.js')
const Env = use('Env')
const Logger = use('Logger')
const Helpers = use('Helpers')
const moment = require('moment');

class CustomerController {
	async index({request, response, auth, view}) {
		let checkAuth = await CheckAuth.get('Customer', 'read', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
		
		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('customer.index', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async datatable({request, response, auth, view, session}) {
		const formData = request.post()
		
		let tableDefinition = {
			sTableName: 'customers',
			sFromSql: "customers inner join users on customers.user_id = users.id",
			sSelectSql: ['customers.id', 'users.fullname', 'users.email', 'customers.phone_number', 'customers.image', 'users.status'],
			aSearchColumns: ['users.fullname', 'users.email', 'customers.phone_number', 'customers.image', 'users.status'],
			dbType: 'postgres',
		}
		
		let queryBuilder = new QueryBuilder(tableDefinition)
		
		let requestQuery = {
			draw: formData.draw,
			columns: formData.columns,
			order: formData.order,
			start: formData.start,
			length: formData.length,
			search: formData.search
		}
		
		let queries = queryBuilder.buildQuery(JSON.parse(JSON.stringify(requestQuery)))

		let select = await Database.raw(queries.select)
		let recordsTotal = await Database.raw(queries.recordsTotal)
		let recordsFiltered = (queries.recordsFiltered) ? await Database.raw(queries.recordsFiltered) : await Database.raw(queries.recordsTotal)

		let selectRow = select.rows

		let fdata = []
		let no = 0
		for(let x in selectRow) {
			fdata.push([
				"<div class='text-center'><input type='checkbox' id='titleCheckdel' style='position: initial; opacity: inherit;' /><input type='hidden' class='deldata' name='item[]' value='"+ selectRow[x]['id'] +"' disabled /></div>\n",
				selectRow[x]['id'],
				selectRow[x]['fullname'],
				selectRow[x]['email'],
				selectRow[x]['phone_number'],
				selectRow[x]['image'],
				selectRow[x]['status'] == 1 ? 'Active' : 'Not Active',
				"<div class='text-center'>\
					<a href='./customer/"+ selectRow[x]['id'] +"/updatestatus' id='"+ selectRow[x]['id'] +"' data-size='small' data-original-title='Update Status'><i class='mdi mdi-refresh text-inverse'></i></a>\
					<a href='./customer/"+ selectRow[x]['id'] +"/edit' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Edit'><i class='fas fa-pencil-alt text-inverse'></i></a>\
					<a href='javascript:void(0);' class='alertdel' id='"+ selectRow[x]['id'] +"' data-toggle='tooltip' data-original-title='Delete'><i class='fas fa-trash text-danger m-l-10'></i></a>\
				</div>\n"
			])
			no++
		}
		
		let data = {
			draw: formData.draw,
			recordsTotal: recordsTotal.rows[0]['count'],
			recordsFiltered: recordsFiltered.rows[0]['count'],
			data: fdata
		}
		
		return data
	}
	
	async create({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		
		let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()

		let meta = {
			title: settings[0]['value'],
			description: settings[1]['value'],
			keywords: settings[2]['value'],
			copyright: settings[3]['value'],
			author: settings[4]['value'],
			url: Env.get('BASE_URL'),
			shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
			bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
			smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
		}
		
		let template = view.render('customer.create', {
			meta: meta
		})
		
		return await minifyHTML.minify(template)
	}
	
	async store({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'create', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const pic = request.file('image', {
		    types: ['image'],
		    size: '5mb'
		})

		let date = moment().format("DD/MM/YYYY");
		let cn = pic.clientName
		let name_pic = date+'-'+cn
		let fix = name_pic.replace(/\//g, "")


		await pic.move(Helpers.publicPath('uploads/customer'), {
		    name: fix
		})

		const { gender, phone_number, date_of_birth, identity_number, address, image, username, password, fullname, email, telephone, status } = request.only(['gender', 'phone_number', 'date_of_birth', 'identity_number', 'address', 'image', 'username', 'password', 'fullname', 'email', 'telephone', 'status'])
		
		let formDataUser = {
			username: username,
			email: email,
			fullname: fullname,
			password: password,
			user_type: 1,
			user_role: 3,
			activation_key: await Hash.make(password),
			status: status,
			created_by: auth.user.id,
			updated_by: auth.user.id
		}

		const rules = {
			username: 'required|unique:users,username',
			email: 'required|email|unique:users,email',
			password: 'required'
		}
		
		const validation = await validateAll(formDataUser, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			let newuser = await User.create(formDataUser)

			await RoleUser.create({
				role_id: 3,
				user_id: newuser.id,
				created_by: auth.user.id,
				updated_by: auth.user.id
			})

			await Customer.create({
				user_id: newuser.id,
				gender: gender,
				phone_number: phone_number,
				date_of_birth: date_of_birth,
				identity_number: identity_number,
				address: address,
				image: fix,
				created_by: auth.user.id,
				updated_by: auth.user.id
			})

			session.flash({ notification: 'Customer added', status: 'success' })
			return response.redirect('/customer')
		}
	}
	
	async edit({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		let customer = await Customer.find(params.id)

		if (customer) {
			let settings = (await Setting.query().orderBy('id', 'asc').fetch()).toJSON()
			
			let meta = {
				title: settings[0]['value'],
				description: settings[1]['value'],
				keywords: settings[2]['value'],
				copyright: settings[3]['value'],
				author: settings[4]['value'],
				url: Env.get('BASE_URL'),
				shortcutIcon: Env.get('BASE_URL') + '/' + settings[9]['value'],
				bigLogo: Env.get('BASE_URL') + '/' + settings[10]['value'],
				smallLogo: Env.get('BASE_URL') + '/' + settings[11]['value']
			}

			let user = await User.query().where('id', customer.user_id).first()
			
			let template = view.render('customer.edit', {
				meta: meta,
				customer: customer,
				user: user.toJSON(),
			})
			
			return await minifyHTML.minify(template)
		} else {
			session.flash({ notification: 'customer not found', status: 'danger' })
			return response.redirect('/customer')
		}
	}
	
	async update({params, request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'update', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}
		let customer = await Customer.find(params.id)

		const pic = request.file('image', {
		    types: ['image'],
		    size: '5mb'
		})

		const { gender, phone_number, date_of_birth, identity_number, address, image, username, password, fullname, email, telephone, status } = request.only(['gender', 'phone_number', 'date_of_birth', 'identity_number', 'address', 'image', 'username', 'password', 'fullname', 'email', 'telephone', 'status'])
		
		let fix =""

		if(pic) {
			let date = moment().format("DD/MM/YYYY");
			let cn = pic.clientName
			let name_pic = date+'-'+cn

			fix = name_pic.replace(/\//g, "")

			if(customer.image != fix) {
				await pic.move(Helpers.publicPath('uploads/customer'), {
				    name: fix
				})
			}
		} else {
			fix = image
		}

		let user
		if (auth.user.id == '1') {
			user = await User.find(params.id)
		} else {
			if (auth.user.id == params.id) {
				user = await User.find(params.id)
			} else {
				user = false
			}
		}
		
		let formDataUser = {
			username: username,
			email: email,
			fullname: fullname
		}

		let rules = {
			fullname: 'required',
			username: `required|unique:users,username,id,${user.id}`,
			email: `required|unique:users,email,id,${user.id}`
		}
		
		const validation = await validateAll(formDataUser, rules)
		if (validation.fails()) {
			session.withErrors(validation.messages())
			return response.redirect('back')
		} else {
			if (user) {
				if (password == '' || password == null) {
					await User.query().where('id', params.id).update({
						username: username,
						email: email,
						fullname: fullname,
						status: status,
						updated_by: auth.user.id
					})
				} else {
					await User.query().where('id', params.id).update({
						username: username,
						email: email,
						fullname: fullname,
						password: await Hash.make(password),
						status: status,
						updated_by: auth.user.id
					})
				}

				await Customer.query().where('user_id', params.id).update({
					gender: gender,
					phone_number: phone_number,
					date_of_birth: date_of_birth,
					identity_number: identity_number,
					address: address,
					image: fix,
					updated_by: auth.user.id
				})

				session.flash({ notification: 'Customer updated', status: 'success' })
				return response.redirect('/customer')
			} else {
				session.flash({ notification: 'Customer cannot be update', status: 'danger' })
				return response.redirect('/customer')
			}
		}
	}
	
	async delete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		let customer = await Customer.find(formData.id)
		if (customer){
			try {
				await customer.delete()
				session.flash({ notification: 'Customer success deleted', status: 'success' })
				return response.redirect('/customer')
			} catch (e) {
				session.flash({ notification: 'Customer cannot be delete', status: 'danger' })
				return response.redirect('/customer')
			}
		} else {
			session.flash({ notification: 'Customer cannot be delete', status: 'danger' })
			return response.redirect('/customer')
		}
	}
	
	async multidelete({request, response, auth, view, session}) {
		let checkAuth = await CheckAuth.get('Customer', 'delete', auth)
		if (!checkAuth) {
			return view.render('error.auth')
		}

		const formData = request.all()
		if (formData.totaldata != '0') {
			for (let i in formData.item) {
				let customer = await Customer.find(formData.item[i])
				try {
					await customer.delete()
				} catch (e) {}
			}
			session.flash({ notification: 'Customer success deleted', status: 'success' })
			return response.redirect('/customer')
		} else {
			session.flash({ notification: 'Customer cannot be deleted', status: 'danger' })
			return response.redirect('/customer')
		}
	}

	async getCustomer({request, response, auth, view, session}) {
		let req = request.get()
		let customers
		if (req.phrase != 'undefined') {
			customers = await Customer.query().where('name', 'LIKE', '%' + req.phrase + '%').limit(20).fetch()
		} else {
			customers = await Customer.query().limit(20).fetch()
		}
		let customer = customers.toJSON()
		
		let data = []
		for(let i in customer){
			data.push({
				id: customer[i]['id'],
				text: customer[i]['name']
			})
		}
		
		return JSON.stringify({ results: data })
	}

	async updateStatus({request, response, auth, view, session}) {
		const formData = request.all()
		let customer = await Customer.find(formData.id)
		if (customer){
			let new_status = ''

			if(customer.status == '1' || customer.status == 1) {
				new_status = 0
			} else {
				new_status = 1
			}

			let formData = {
				status: new_status,

				updated_by: auth.user.id
			}
			await customer.update(formData)
			session.flash({ notification: 'Status success updated', status: 'success' })
			return response.redirect('/customer')
		} else {
			session.flash({ notification: 'Status cannot be update', status: 'danger' })
			return response.redirect('/customer')
		}
	}
}

module.exports = CustomerController
