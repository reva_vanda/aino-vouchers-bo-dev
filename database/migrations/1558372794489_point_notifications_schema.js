'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PointNotificationsSchema extends Schema {
  up () {
    this.create('point_notifications', (table) => {
      	table.increments()
		table.integer('user_id')
		table.string('principal_id')
		table.string('title')
		table.string('message')
		table.string('type')
		table.integer('created_by', 10).notNullable()
		table.integer('updated_by', 10).notNullable()
      	table.timestamps()
    })
  }

  down () {
    this.drop('point_notifications')
  }
}

module.exports = PointNotificationsSchema
