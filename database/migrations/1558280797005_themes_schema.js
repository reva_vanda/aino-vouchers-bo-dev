'use strict'

const Schema = use('Schema')

class ThemesSchema extends Schema {
		up () {
			this.create('themes', (table) => {
			table.increments()
			table.integer('user_id')
			table.string('principal_id')
			table.string('principal_code')
			table.string('logo')
			table.string('banner')
			table.string('color_one')
			table.string('color_two')
			table.string('color_three')
			table.string('color_four')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
			})
		}

		down () {
			this.drop('themes')
		}
}

module.exports = ThemesSchema
