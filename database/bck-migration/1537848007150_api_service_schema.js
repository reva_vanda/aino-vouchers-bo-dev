'use strict'

const Schema = use('Schema')

class ApiServicesSchema extends Schema {
	up () {
		this.create('api_services', (table) => {
			table.increments()
			table.string('client_name')
			table.string('client_key')
			table.string('client_secret')
			table.text('permissions')
			table.integer('status')
			table.integer('created_by', 10).notNullable()
			table.integer('updated_by', 10).notNullable()
			table.timestamps()
		})
	}

	down () {
		this.drop('api_services')
	}
}

module.exports = ApiServicesSchema
