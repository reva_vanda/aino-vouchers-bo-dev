'use strict'

const Schema = use('Schema')

class RoleUserSchema extends Schema {
	up () {
		this.create('role_user', (table) => {
			table.increments()
			table.integer('role_id', 10)
			table.integer('user_id', 10)
			table.integer('created_by', 10).defaultTo(1)
			table.integer('updated_by', 10).defaultTo(1)
			table.timestamps()
		})
	}

	down () {
		this.drop('role_user')
	}
}

module.exports = RoleUserSchema
