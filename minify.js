const compressor = require('node-minify')

compressor.minify({
	compressor: 'uglify-es',
	input: 'public/admin/js/main.js',
	output: 'public/admin/js/main.min.js',
	sync: true,
	callback: function(err, min) {
		console.log('main.js')
	}
})

compressor.minify({
	compressor: 'clean-css',
	input: 'public/admin/css/main.css',
	output: 'public/admin/css/main.min.css',
	sync: true,
	callback: function(err, min) {
		console.log('main.css')
	}
})